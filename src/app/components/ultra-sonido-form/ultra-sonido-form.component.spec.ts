import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UltraSonidoFormComponent } from './ultra-sonido-form.component';

describe('UltraSonidoFormComponent', () => {
  let component: UltraSonidoFormComponent;
  let fixture: ComponentFixture<UltraSonidoFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UltraSonidoFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UltraSonidoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

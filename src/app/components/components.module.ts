import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TomaTargetComponent } from './toma-target/toma-target.component';
import { UltraSonidoFormComponent } from './ultra-sonido-form/ultra-sonido-form.component';



@NgModule({
  declarations: [TomaTargetComponent, UltraSonidoFormComponent],
  imports: [
    CommonModule
  ]
})
export class ComponentsModule { }

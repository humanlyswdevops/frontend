import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UltraSonidoRoutingModule } from './ultra-sonido-routing.module';
import { UltraSonidoFormComponent } from './ultra-sonido-form/ultra-sonido-form.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDividerModule } from '@angular/material/divider';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [UltraSonidoFormComponent],
  imports: [
    CommonModule,
    UltraSonidoRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatDividerModule,
    FormsModule,
    HttpClientModule
  ]
})
export class UltraSonidoModule { }

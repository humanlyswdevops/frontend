import { Component, OnInit } from '@angular/core';
import { UltraSonidoService } from 'src/app/services/ultra-sonido.service';

@Component({
  selector: 'app-ultra-sonido-form',
  templateUrl: './ultra-sonido-form.component.html',
  styleUrls: ['./ultra-sonido-form.component.scss']
})
export class UltraSonidoFormComponent implements OnInit {

  public status: string = 'init';

  constructor(private uss:UltraSonidoService) { }



  ngOnInit(): void {
  }

  testing(){
    this.status = 'oneMoment';
    this.uss.test().subscribe(res=>{
      console.log(res);
      console.log('ya valio');
      
      this.status = 'success';
      console.log(this.status);
    },err=>{
      console.log(err);
      
      this.status = 'errros';
    })

  }

}

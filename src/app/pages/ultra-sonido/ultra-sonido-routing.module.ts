import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UltraSonidoFormComponent } from './ultra-sonido-form/ultra-sonido-form.component';
import { UltraSonidoComponent } from './ultra-sonido.component';

const routes: Routes = [
   {path:'',component:UltraSonidoComponent},
   {path:'Registro',component:UltraSonidoFormComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UltraSonidoRoutingModule { }

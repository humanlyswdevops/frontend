import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportesComponent } from './reportes/reportes.component';
import { RutasComponent } from './rutas/rutas.component';
import { TomasComponent } from './tomas/tomas.component';
import { LayoutComponent } from './_layout/layout.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: () =>
          import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
      },
      {
        path: 'builder',
        loadChildren: () =>
          import('./builder/builder.module').then((m) => m.BuilderModule),
      },
      {path:'Tomas',component:TomasComponent},
      {
        path:'Ultra-Sonido',
        loadChildren:() => import('./ultra-sonido/ultra-sonido.module').then((m)=>m.UltraSonidoModule),
      },
      {path:'Rutas',component:RutasComponent},
      {path:'Reportes',component:ReportesComponent},
      {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full',
      },
      {
        path: '**',
        redirectTo: 'error/404',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule { }

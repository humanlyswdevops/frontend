import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UltraSonidoService {

  constructor(private http:HttpClient) { }

  test(){
    return this.http.get('https://jsonplaceholder.typicode.com/todos/1');    
  }

}
